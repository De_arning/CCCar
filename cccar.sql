/*
Navicat MySQL Data Transfer

Source Server         : root
Source Server Version : 50514
Source Host           : localhost:3307
Source Database       : cccar

Target Server Type    : MYSQL
Target Server Version : 50514
File Encoding         : 65001

Date: 2021-09-14 17:40:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for allocation
-- ----------------------------
DROP TABLE IF EXISTS `allocation`;
CREATE TABLE `allocation` (
`allocateTime`  datetime NULL DEFAULT NULL ,
`allocation_id`  int(11) NOT NULL AUTO_INCREMENT ,
`car_id`  int(11) NULL DEFAULT NULL ,
`net_in_id`  int(11) NULL DEFAULT NULL ,
`net_out_id`  int(11) NULL DEFAULT NULL ,
PRIMARY KEY (`allocation_id`),
FOREIGN KEY (`car_id`) REFERENCES `car_info` (`car_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
FOREIGN KEY (`net_out_id`) REFERENCES `net_info` (`net_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
FOREIGN KEY (`net_in_id`) REFERENCES `net_info` (`net_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
INDEX `FK_allocate` (`car_id`) USING BTREE ,
INDEX `FK_in` (`net_out_id`) USING BTREE ,
INDEX `FK_out` (`net_in_id`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=11

;

-- ----------------------------
-- Records of allocation
-- ----------------------------
BEGIN;
INSERT INTO `allocation` VALUES ('2021-09-08 20:16:45', '1', '1', '3', '1'), ('2021-09-08 20:17:05', '2', '1', '3', '1'), ('2021-09-08 20:17:15', '3', '1', '1', '3'), ('2021-09-14 10:23:55', '4', '1', '3', '1'), ('2021-09-14 10:24:12', '5', '1', '3', '1'), ('2021-09-14 10:26:57', '6', '1', '1', '3'), ('2021-09-14 10:27:10', '7', '1', '1', '3'), ('2021-09-14 10:27:14', '8', '1', '1', '3'), ('2021-09-14 13:50:15', '9', '4', '1', '2'), ('2021-09-14 16:00:21', '10', '3', '2', '1');
COMMIT;

-- ----------------------------
-- Table structure for car_category
-- ----------------------------
DROP TABLE IF EXISTS `car_category`;
CREATE TABLE `car_category` (
`category_Id`  int(11) NOT NULL AUTO_INCREMENT ,
`category_Name`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`category_Description`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`category_Id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=8

;

-- ----------------------------
-- Records of car_category
-- ----------------------------
BEGIN;
INSERT INTO `car_category` VALUES ('1', 'a', 'this is for test'), ('2', '经济型', '大众'), ('3', '商务型', '你猜猜'), ('4', 'SUV', '你猜猜'), ('5', '豪华型', '你猜猜'), ('6', 'test1', 'testtest特特殊'), ('7', 'test刷新车类', 'test刷新车类');
COMMIT;

-- ----------------------------
-- Table structure for car_info
-- ----------------------------
DROP TABLE IF EXISTS `car_info`;
CREATE TABLE `car_info` (
`car_id`  int(11) NOT NULL AUTO_INCREMENT ,
`net_id`  int(11) NULL DEFAULT NULL ,
`type_Id`  int(11) NULL DEFAULT NULL ,
`license`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`car_status`  int(11) NULL DEFAULT NULL ,
PRIMARY KEY (`car_id`),
FOREIGN KEY (`net_id`) REFERENCES `net_info` (`net_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
FOREIGN KEY (`type_Id`) REFERENCES `car_type` (`type_Id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
INDEX `FK_exists` (`net_id`) USING BTREE ,
INDEX `FK_have` (`type_Id`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=10

;

-- ----------------------------
-- Records of car_info
-- ----------------------------
BEGIN;
INSERT INTO `car_info` VALUES ('1', '1', '1', '浙A-88888', '0'), ('3', '2', '2', '浙A-88882', '1'), ('4', '1', '1', '1', '1'), ('7', '1', '3', '12', '1'), ('8', '1', '4', '2', '1'), ('9', '1', '1', '123', '-1');
COMMIT;

-- ----------------------------
-- Table structure for car_type
-- ----------------------------
DROP TABLE IF EXISTS `car_type`;
CREATE TABLE `car_type` (
`type_Id`  int(11) NOT NULL AUTO_INCREMENT ,
`type_Name`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`category_Id`  int(11) NULL DEFAULT NULL ,
`brand`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`displacement`  decimal(10,2) NULL DEFAULT NULL ,
`gear`  int(11) NULL DEFAULT NULL ,
`seat_num`  int(11) NULL DEFAULT NULL ,
`price`  decimal(10,2) NULL DEFAULT NULL ,
`pic`  varchar(10000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`type_Id`),
FOREIGN KEY (`category_Id`) REFERENCES `car_category` (`category_Id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
INDEX `FK_include` (`category_Id`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=5

;

-- ----------------------------
-- Records of car_type
-- ----------------------------
BEGIN;
INSERT INTO `car_type` VALUES ('1', 'aaa', '1', 'test', '1.00', '1', '1', '1.00', 'test'), ('2', '萨日朗', '2', 'test', '1.00', '1', '1', '2.00', 'https://tse1-mm.cn.bing.net/th/id/R-C.250f95855aec49c974acb36b3ed32571?rik=BgcOI1PX1fb%2bww&riu=http%3a%2f%2fwww.desktx.com%2fd%2ffile%2fwallpaper%2fscenery%2f20170120%2f387b1a5181ebeddbec90fd5f19e606ce.jpg&ehk=Feb%2bz1leZKOVuTS20av3z7LKELRP0HH277cc6aSrAeI%3d&risl=&pid=ImgRaw&r=0'), ('3', '2', '3', '1', '1.00', '1', '1', '1.00', '1'), ('4', '1', '4', '1', '1.00', '1', '1', '1.00', '1');
COMMIT;

-- ----------------------------
-- Table structure for coupon
-- ----------------------------
DROP TABLE IF EXISTS `coupon`;
CREATE TABLE `coupon` (
`coupon_id`  int(11) NOT NULL AUTO_INCREMENT ,
`content`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`creditAmount`  int(11) NULL DEFAULT NULL ,
`startDate`  datetime NULL DEFAULT NULL ,
`endDate`  datetime NULL DEFAULT NULL ,
`user_id`  int(11) NULL DEFAULT NULL ,
`isUsed`  int(4) NULL DEFAULT NULL ,
`net_id`  int(11) NULL DEFAULT NULL ,
PRIMARY KEY (`coupon_id`),
FOREIGN KEY (`net_id`) REFERENCES `net_info` (`net_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
INDEX `FK_shuyu` (`net_id`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=7

;

-- ----------------------------
-- Records of coupon
-- ----------------------------
BEGIN;
INSERT INTO `coupon` VALUES ('1', 'test', '1', '2021-09-01 00:00:00', '2021-10-01 00:00:00', '1', '1', '1'), ('2', 'test', '10', '2021-09-13 15:37:44', '2021-10-13 15:37:47', null, '0', '1'), ('3', '什么玩意', '11', '2021-09-14 10:32:56', '2021-10-14 10:32:58', null, '0', '1'), ('5', '测试添加属于当前网点优惠券', '10', '2021-09-14 11:25:49', '2021-10-14 11:25:52', null, '0', '1'), ('6', 'test', '1', '2021-09-14 11:30:36', '2021-09-14 11:30:37', null, '0', '1');
COMMIT;

-- ----------------------------
-- Table structure for discount_info
-- ----------------------------
DROP TABLE IF EXISTS `discount_info`;
CREATE TABLE `discount_info` (
`dicount_id`  int(11) NOT NULL AUTO_INCREMENT ,
`net_id`  int(11) NULL DEFAULT NULL ,
`type_Id`  int(11) NULL DEFAULT NULL ,
`discountAmount`  int(11) NULL DEFAULT NULL ,
`discountNum`  int(11) NULL DEFAULT NULL ,
`startDate`  datetime NULL DEFAULT NULL ,
`endDate`  datetime NULL DEFAULT NULL ,
PRIMARY KEY (`dicount_id`),
FOREIGN KEY (`type_Id`) REFERENCES `car_type` (`type_Id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
FOREIGN KEY (`net_id`) REFERENCES `net_info` (`net_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
INDEX `FK_bePromotionby` (`type_Id`) USING BTREE ,
INDEX `FK_hold` (`net_id`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=3

;

-- ----------------------------
-- Records of discount_info
-- ----------------------------
BEGIN;
INSERT INTO `discount_info` VALUES ('1', '1', '1', '1', '10', '2021-09-01 15:31:40', '2021-09-30 15:31:40'), ('2', '1', '2', '11', '10', '2021-09-13 15:34:59', '2021-10-13 15:35:03');
COMMIT;

-- ----------------------------
-- Table structure for employee
-- ----------------------------
DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee` (
`employee_id`  int(11) NOT NULL AUTO_INCREMENT ,
`net_id`  int(11) NULL DEFAULT NULL ,
`name`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`password`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`employee_id`),
FOREIGN KEY (`net_id`) REFERENCES `net_info` (`net_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
INDEX `FK_employ` (`net_id`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=3

;

-- ----------------------------
-- Records of employee
-- ----------------------------
BEGIN;
INSERT INTO `employee` VALUES ('1', null, 'admin', '123'), ('2', '1', 'user1', '1234');
COMMIT;

-- ----------------------------
-- Table structure for net_info
-- ----------------------------
DROP TABLE IF EXISTS `net_info`;
CREATE TABLE `net_info` (
`net_id`  int(11) NOT NULL AUTO_INCREMENT ,
`net_name`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`city`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`address`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`phone`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`net_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=9

;

-- ----------------------------
-- Records of net_info
-- ----------------------------
BEGIN;
INSERT INTO `net_info` VALUES ('1', 'net1', 'hangzhou', 'gongshu', '110'), ('2', 'net3', 'hanzghou', '成华大道', '110'), ('3', 'net2', 'hangzhou', '123', '123'), ('6', 'net2', 'hangzhou', '123', '123'), ('7', 'net5', 'nima', 'wotm', '123'), ('8', 'net6', 'hangzhou', '西湖区', '1101');
COMMIT;

-- ----------------------------
-- Table structure for scrap
-- ----------------------------
DROP TABLE IF EXISTS `scrap`;
CREATE TABLE `scrap` (
`scrap_id`  int(11) NOT NULL AUTO_INCREMENT ,
`employee_id`  int(11) NULL DEFAULT NULL ,
`car_id`  int(11) NULL DEFAULT NULL ,
`scrapTime`  datetime NULL DEFAULT NULL ,
`description`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`scrap_id`),
FOREIGN KEY (`car_id`) REFERENCES `car_info` (`car_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
INDEX `FK_dead` (`car_id`) USING BTREE ,
INDEX `FK_manage` (`employee_id`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=12

;

-- ----------------------------
-- Records of scrap
-- ----------------------------
BEGIN;
INSERT INTO `scrap` VALUES ('11', '1', '9', '2021-09-14 13:56:02', 'test');
COMMIT;

-- ----------------------------
-- Table structure for tbl_order
-- ----------------------------
DROP TABLE IF EXISTS `tbl_order`;
CREATE TABLE `tbl_order` (
`order_id`  int(11) NOT NULL AUTO_INCREMENT ,
`coupon_id`  int(11) NULL DEFAULT NULL ,
`net_borrow_id`  int(11) NULL DEFAULT NULL ,
`car_Id`  int(11) NULL DEFAULT NULL ,
`net_return_id`  int(11) NULL DEFAULT NULL ,
`user_id`  int(11) NULL DEFAULT NULL ,
`borrowDate`  datetime NULL DEFAULT NULL ,
`returnDate`  datetime NULL DEFAULT NULL ,
`borrowDuration`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`initial_amount`  decimal(10,2) NULL DEFAULT NULL ,
`original_amount`  decimal(10,2) NULL DEFAULT NULL ,
`totalAmount`  decimal(10,2) NULL DEFAULT NULL ,
`order_status`  int(11) NULL DEFAULT NULL ,
PRIMARY KEY (`order_id`),
FOREIGN KEY (`coupon_id`) REFERENCES `coupon` (`coupon_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
FOREIGN KEY (`car_Id`) REFERENCES `car_info` (`car_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
FOREIGN KEY (`user_id`) REFERENCES `user_info` (`user_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
FOREIGN KEY (`net_return_id`) REFERENCES `net_info` (`net_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
FOREIGN KEY (`net_borrow_id`) REFERENCES `net_info` (`net_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
INDEX `FK_canUseAt` (`coupon_id`) USING BTREE ,
INDEX `FK_orders` (`user_id`) USING BTREE ,
INDEX `FK_recives` (`net_return_id`) USING BTREE ,
INDEX `FK_return` (`net_borrow_id`) USING BTREE ,
INDEX `FK_get` (`car_Id`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=50

;

-- ----------------------------
-- Records of tbl_order
-- ----------------------------
BEGIN;
INSERT INTO `tbl_order` VALUES ('1', null, '1', '1', '1', '1', '2021-09-07 16:03:06', '2021-09-13 19:30:19', '147小时27分13秒', '1.00', '148.00', '14.80', '1'), ('18', null, '1', '1', '1', '1', '2021-09-08 16:13:24', '2021-09-13 19:30:54', '123小时17分30秒', '1.00', '124.00', '12.40', '1'), ('20', null, '1', '1', '1', '1', '2021-09-08 20:16:20', '2021-09-13 19:30:28', '119小时14分8秒', '1.00', '120.00', '12.00', '1'), ('21', null, '1', '1', '1', '1', '2021-09-08 20:16:27', '2021-09-13 14:31:38', '114小时15分11秒', '1.00', '115.00', '115.00', '1'), ('26', null, '1', '1', '1', '1', '2021-09-08 20:17:31', '2021-09-13 14:31:28', '114小时13分57秒', '1.00', '115.00', '115.00', '1'), ('27', null, '1', '1', '1', '1', '2021-09-08 20:17:33', '2021-09-13 14:31:41', '114小时14分8秒', '1.00', '115.00', '115.00', '1'), ('28', null, '1', '1', '1', null, '2021-09-08 20:25:35', '2021-09-13 14:31:33', '114小时5分58秒', '1.00', '115.00', '115.00', '1'), ('31', null, '1', '1', '1', null, '2021-09-13 14:25:15', '2021-09-13 14:26:48', '0小时1分33秒', '1.00', '1.00', '1.00', '1'), ('32', null, '1', '1', '1', null, '2021-09-13 19:31:20', '2021-09-13 19:31:24', '0小时0分4秒', '1.00', '1.00', '0.10', '1'), ('33', null, '1', '3', '1', null, '2021-09-13 19:31:36', '2021-09-13 19:31:39', '0小时0分3秒', '2.00', '2.00', '0.22', '1'), ('34', null, '1', '1', '1', null, '2021-09-13 19:35:41', '2021-09-13 19:36:25', '0小时0分44秒', '1.00', '1.00', '0.10', '1'), ('35', null, '1', '3', '1', null, '2021-09-13 19:36:33', '2021-09-13 19:39:37', '0小时3分4秒', '2.00', '2.00', '0.22', '1'), ('36', null, '1', '1', '1', null, '2021-09-13 19:39:25', '2021-09-13 19:39:35', '0小时0分10秒', '1.00', '1.00', '0.10', '1'), ('37', null, '1', '1', '1', null, '2021-09-13 20:02:14', '2021-09-13 20:02:30', '0小时0分16秒', '1.00', '1.00', '0.10', '1'), ('38', null, '1', '1', '1', '1', '2021-09-14 10:09:20', '2021-09-14 10:21:12', '0小时11分52秒', '1.00', '1.00', '0.10', '1'), ('39', null, '1', '3', '1', '1', '2021-09-14 10:11:57', '2021-09-14 10:21:15', '0小时9分18秒', '2.00', '2.00', '0.22', '1'), ('40', null, '1', '1', '1', '1', '2021-09-14 10:23:31', '2021-09-14 10:23:33', '0小时0分2秒', '1.00', '1.00', '0.10', '1'), ('41', null, '1', '1', '3', '1', '2021-09-14 10:23:49', '2021-09-14 10:24:12', '0小时0分23秒', '1.00', '1.00', '0.10', '1'), ('42', null, '3', '1', '3', '1', '2021-09-14 10:24:07', '2021-09-14 10:24:16', '0小时0分9秒', '1.00', '1.00', '1.00', '1'), ('43', null, '3', '1', '3', '1', '2021-09-14 10:24:22', '2021-09-14 10:24:25', '0小时0分3秒', '1.00', '1.00', '1.00', '1'), ('44', null, '3', '1', '1', '1', '2021-09-14 10:26:55', '2021-09-14 10:27:14', '0小时0分19秒', '1.00', '1.00', '1.00', '1'), ('45', null, '2', '4', '1', '1', '2021-09-14 13:50:03', '2021-09-14 13:50:15', '0小时0分12秒', '1.00', '1.00', '1.00', '1'), ('46', null, '1', '1', '1', '1', '2021-09-14 13:50:58', '2021-09-14 13:51:05', '0小时0分7秒', '1.00', '1.00', '0.10', '1'), ('47', null, '1', '1', '1', null, '2021-09-14 14:58:00', '2021-09-14 15:00:54', '0小时2分54秒', '1.00', '1.00', '0.10', '1'), ('48', null, '1', '1', '1', null, '2021-09-14 15:08:58', null, null, '1.00', null, null, '0'), ('49', null, '1', '3', '2', '1', '2021-09-14 16:00:11', '2021-09-14 16:00:21', '0小时0分10秒', '2.00', '2.00', '0.22', '1');
COMMIT;

-- ----------------------------
-- Table structure for user_info
-- ----------------------------
DROP TABLE IF EXISTS `user_info`;
CREATE TABLE `user_info` (
`user_id`  int(11) NOT NULL AUTO_INCREMENT ,
`name`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`sex`  int(11) NULL DEFAULT NULL ,
`password`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`phone`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`email`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`city`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`registrationTime`  datetime NULL DEFAULT NULL ,
PRIMARY KEY (`user_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=2

;

-- ----------------------------
-- Records of user_info
-- ----------------------------
BEGIN;
INSERT INTO `user_info` VALUES ('1', 'VIP', '1', '124', '110', '1@qq.com', 'hangzhou', '2021-09-05 09:29:59');
COMMIT;

-- ----------------------------
-- Auto increment value for allocation
-- ----------------------------
ALTER TABLE `allocation` AUTO_INCREMENT=11;

-- ----------------------------
-- Auto increment value for car_category
-- ----------------------------
ALTER TABLE `car_category` AUTO_INCREMENT=8;

-- ----------------------------
-- Auto increment value for car_info
-- ----------------------------
ALTER TABLE `car_info` AUTO_INCREMENT=10;

-- ----------------------------
-- Auto increment value for car_type
-- ----------------------------
ALTER TABLE `car_type` AUTO_INCREMENT=5;

-- ----------------------------
-- Auto increment value for coupon
-- ----------------------------
ALTER TABLE `coupon` AUTO_INCREMENT=7;

-- ----------------------------
-- Auto increment value for discount_info
-- ----------------------------
ALTER TABLE `discount_info` AUTO_INCREMENT=3;

-- ----------------------------
-- Auto increment value for employee
-- ----------------------------
ALTER TABLE `employee` AUTO_INCREMENT=3;

-- ----------------------------
-- Auto increment value for net_info
-- ----------------------------
ALTER TABLE `net_info` AUTO_INCREMENT=9;

-- ----------------------------
-- Auto increment value for scrap
-- ----------------------------
ALTER TABLE `scrap` AUTO_INCREMENT=12;

-- ----------------------------
-- Auto increment value for tbl_order
-- ----------------------------
ALTER TABLE `tbl_order` AUTO_INCREMENT=50;

-- ----------------------------
-- Auto increment value for user_info
-- ----------------------------
ALTER TABLE `user_info` AUTO_INCREMENT=2;
